/**
 * Created by longsiwei on 17/12/25.
 */
import React from 'react';
import {Form, Modal, Input} from 'antd';

const FormItem = Form.Item;

const userEdit = Form.create()((props) => {

  const {visible, onCancel, onSave, form, record} = props;

  console.log("user edit record: " + JSON.stringify(record));

  const {getFieldDecorator} = form;

  let {id, name, username, email} = {id: "", name: "", username: "", email: ""};

  if(record){
    id = record.id;
    name = record.name;
    username = record.username;
    email = record.email;
  }

  const okHandler = () => {
    props.form.validateFields((err, values) => {
      console.log("okHandler: " + values);
      if(!err){
        if(id){
          onSave({id, ...values});
        }else{
          onSave(values);
        }

        onCancel();
      }
    });
  };

  return (
    <Modal visible={visible} onCancel={onCancel} onOk={okHandler} title="Create a new collection" okText="Create">
      <Form layout="vertical" onSubmit={okHandler}>
        <FormItem>
          {
            getFieldDecorator('name', {
              initialValue: name,
              rules: [{ required: true, message: 'Please input the name of user!' }]
            })(
              <Input />
            )
          }
        </FormItem>
        <FormItem>
          {
            getFieldDecorator('username', {
              initialValue: username
            })(
              <Input />
            )
          }
        </FormItem>
        <FormItem>
          {
            getFieldDecorator('email', {
              initialValue: email
            })(
              <Input />
            )
          }
        </FormItem>
      </Form>
    </Modal>
  );
});

export default userEdit;
