/**
 * Created by longsiwei on 17/12/24.
 */
import React from 'react';
import {Table, Button, Pagination, Popconfirm} from 'antd';
import {connect} from 'dva';
import UserEdit from './UserEdit';
import * as Const from '../utils/constants';
import {routerRedux} from 'dva/router';
import queryString from 'query-string';

const userList = ({dispatch, list, total, showModal, page, record}) => {
  console.log("dispath: " + typeof (dispatch));
  console.log("user list props: " + JSON.stringify(list));

  const columns = [
    {
      title: '名字',
      key: 'name',
      dataIndex: 'name'
    },
    {
      title: '用户名 ',
      key: 'username',
      dataIndex: 'username'
    },
    {
      title: '邮箱 ',
      key: 'email',
      dataIndex: 'email'
    },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <a onClick={onEdit.bind(null, record)}>修改</a>&nbsp;
          <Popconfirm title="是否删除" onConfirm={onDelete.bind(null, record.id)}>
            <a href="javascript:void(0)">删除</a>
          </Popconfirm>
        </span>
      )
    }
  ];

  const createModal = () => {
    dispatch({
      type: 'users/showModal',
      payload: {showModal: true}
    });
  };

  const saveFormRef = (form) => {
    this.form = form;
  }

  const onCreateCancel = () => {
    dispatch({
      type: 'users/showModal',
      payload: {showModal: false}
    });
  }

  const onSave = (values) => {
    dispatch({
      type: 'users/insert',
      payload: values
    });
  }

  const changePage = (page) => {
    console.log("changePage page: " + page);
    dispatch(routerRedux.push({
      pathname: '/users',
      search: queryString.stringify({page})
    }));
  }

  const onDelete = (id) => {
    console.log("onDelete id: " + id);
    dispatch({
      type: 'users/delete',
      payload: id
    });
  }

  const onEdit = (record) => {
    dispatch({
      type: 'users/edit',
      payload: {record, showModal: true}
    });
  }

  return (
    <div>
      <Button type="primary" onClick={createModal} >创建</Button>
      <UserEdit onCancel={onCreateCancel} onSave={onSave} visible={showModal} record={record} />
      <Table rowKey={record => record.id} columns={columns} dataSource={list.content} pagination={false} />
      <Pagination defaultCurrent={1} current={page} total={total} pageSize={Const.PAGE_SIZE} onChange={changePage} />
    </div>
  );
}

const mapStateToProps = (state) => {
  console.log("mapstatetoprops: " + JSON.stringify(state));
  return {...state.users};
};

export default connect(mapStateToProps)(userList);
