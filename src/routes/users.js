import React from 'react';
import {connect} from 'dva';

import UserList from '../components/Users';

const Users = (props) => {
  return(
    <div>
      <h2>User List</h2>
      <UserList />
    </div>
  );
};

export default connect()(Users);
