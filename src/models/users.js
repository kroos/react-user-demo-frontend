import * as userService from '../services/users';
import queryString from 'query-string';

export default {
  namespace: 'users',
  state: {
    list: [],
    total: 0
  },
  reducers: {
    save(state, {payload: {data: list, total, showModal, page}}){
      return {...state, list, total, showModal, page};
    },
    showModal(state, {payload: {showModal}}){
      console.log("showModal: " + showModal);
      return {...state, showModal};
    },
    edit(state, {payload: {record: record, showModal}}){
      return {...state, record, showModal};
    }
  },
  effects: {
    *fetch({payload: {page}}, {call, put}){
      const {data, headers} = yield call(userService.fetch, {page});
      console.log("fetch data: " + JSON.stringify(data));
      yield put({type: 'save', payload: { data, total: data.totalElements, showModal: false, page: data.number + 1 }});
    },
    *insert({payload: values}, {call, put}){
      yield call(userService.save, values);
      yield put({type: 'reload', payload: {}});
    },
    *reload({payload: {}}, {select, put}){
      console.log("reload triggered.");
      let page = yield select(state => state.users.page);
      console.log("reload page: " + page);
      yield put({type: 'fetch', payload: {page: page}});
      yield put({type: 'fetch', payload: {page: page}});
    },
    *delete({payload: id}, {call, put}){
      yield call(userService.remove, id);
      yield put({type: 'fetch', payload: {page: 1}});
    }
  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname, search }) => {
        let query = queryString.parse(search);
        if (pathname === '/users') {
          dispatch({ type: 'fetch', payload: query });
        }
      });
    }
  },
};
