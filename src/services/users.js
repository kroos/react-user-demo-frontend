/**
 * Created by longsiwei on 17/12/24.
 */
import request from '../utils/request';

export function fetch({page = 1}){
  console.log("fetch page: " + page);
  // return request('/api/users?_page=${page}&_limit=5');
  return request('/api/users/page/list/' + page);
};

export function save(values){
  console.log("service save: " + JSON.stringify(values));
  request('/api/users/create', {
    method: 'POST',
    body: JSON.stringify(values)
  });
};

export function remove(id){
  return request('/api/users/delete/' + id, {
    method: 'DELETE'
  });
}
